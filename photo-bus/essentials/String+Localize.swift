//
//  PhotoCollectionCell.swift
//  photo-bus
//
//  Created by Lee Irvine on 3/11/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import Foundation

extension String {
    
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
    func paranthesized() -> String {
        return "(\(self))"
    }
    
}
