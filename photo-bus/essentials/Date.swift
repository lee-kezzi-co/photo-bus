//
//  Date.swift
//  photo-bus
//
//  Created by Irvine, Lee on 5/3/19.
//  Copyright © 2019 Kezzi.co. All rights reserved.
//

import UIKit

extension Date {
    struct Formatter {
        static let iso8601DateAndTime: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXXXX"
            return formatter
        }()

        static let iso8601DateOnly: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()

    }
    

    var iso8601DateAndTime: String {
        return Formatter.iso8601DateAndTime.string(from: self)
    }
    
    var iso8601DateOnly:String {
        return Formatter.iso8601DateOnly.string(from: self)
    }
}

