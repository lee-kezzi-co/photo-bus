//
//  PhotoCollectionCell.swift
//  photo-bus
//
//  Created by Lee Irvine on 3/11/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import UIKit

//
//  PhotoCell.swift
//  photo-roundup
//
//  Created by Lee Irvine on 8/13/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
//    @IBOutlet weak var spinner: DialView!

    static var id: Int = 0
    var id: Int = 0

    class func nextId() -> Int {
        id = id + 1
        return id
    }
}
