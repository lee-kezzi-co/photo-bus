//
//  PhotoCollectionViewControllerRepresentable.swift
//  photo-bus
//
//  Created by Lee Irvine on 3/11/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import SwiftUI
import UIKit

struct PhotoCollectionViewControllerRepresentable: UIViewControllerRepresentable {
    var controllers: [UIViewController]
    
//    @Binding var currentPage: Int

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIViewController(context: Context) -> PhotoCollectionViewController {
        let vc = UIStoryboard(name: "photo-collection", bundle: nil).instantiateViewController(identifier: "photo-collection") as! PhotoCollectionViewController

        return vc
    }

    func updateUIViewController(_ collectionVc: PhotoCollectionViewController, context: Context) {
        collectionVc.collectionView.reloadData()
    }

    class Coordinator: NSObject {
        var parent: PhotoCollectionViewControllerRepresentable

        init(_ photoCollectionViewController: PhotoCollectionViewControllerRepresentable) {
            self.parent = photoCollectionViewController
        }

    }

}
