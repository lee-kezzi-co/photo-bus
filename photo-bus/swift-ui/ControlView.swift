//
//  ControlView.swift
//  photo-bus
//
//  Created by Lee Irvine on 3/11/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import SwiftUI
struct ControlView: View {
    var body: some View {
        HStack {
            QualityPickerView()
            Button(action: { }) {
                Text("Export")
            }
        }
    }
}
