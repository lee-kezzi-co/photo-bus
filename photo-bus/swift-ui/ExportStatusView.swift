//
//  ExportStatusView.swift
//  photo-bus
//
//  Created by Lee Irvine on 3/11/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import SwiftUI

struct ExportStatusView: View {
    
    @Binding var numberOfPhotosToExport: Int
    
    var body: some View {
        Text("\(numberOfPhotosToExport)")
    }

}
