//
//  ContentView.swift
//  photo-bus
//
//  Created by Lee Irvine on 3/11/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            
            PhotoCollectionViewControllerRepresentable(controllers: [ UIViewController() ])
            
            ControlView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
